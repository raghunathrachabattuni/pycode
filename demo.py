import cv2 as C
import numpy as np

blue=np.uint8([[[255,0,0 ]]])
hsv_blue = C.cvtColor(blue,C.COLOR_BGR2HSV)
print(hsv_blue)
step=2;
lower_blue=np.array([max(hsv_blue[0,0,0]-step,0),10,10])
upper_blue=np.array([min(179,hsv_blue[0,0,0]+step),255,255])

red=np.uint8([[[0,0,255 ]]])
hsv_red = C.cvtColor(red,C.COLOR_BGR2HSV)
print(hsv_red)
lower_red=np.array([max(hsv_red[0,0,0]-step,0),10,10])
upper_red=np.array([min(179,hsv_red[0,0,0]+step),255,255])


green=np.uint8([[[0,255,0 ]]])
hsv_green = C.cvtColor(green,C.COLOR_BGR2HSV)
print(hsv_green)
lower_green=np.array([max(hsv_green[0,0,0]-step,0),10,10])
upper_green=np.array([min(179,hsv_green[0,0,0]+step),255,255])

# cap=C.VideoCapture('testV.mp4')
# while(cap.isOpened):

	# ret,frame = cap.read()#to read a frame
	# gray=C.cvtColor(frame,C.COLOR_BGR2GRAY)#to convert from BGR to gray space
	# hsv=C.cvtColor(frame,C.COLOR_BGR2HSV)
	# #lower_blue = np.array([110,50,50])#define lower boundary
	# #upper_blue = np.array([130,255,255])
	# msk = C.inRange(hsv, lower_blue, upper_blue)#select the values in the range
	# res = C.bitwise_and(frame,frame, mask= msk)#pick only those
	
	# #C.imshow('gray',gray) #to display a frame
	# #C.imshow('frame',frame)
	# #C.imshow('mask',msk)
	# C.imshow('res',res)
	# k = C.waitKey(20) & 0xFF # wait for 20ms or key stroke
	
	# #if C.waitKey(50) & 0xFF == ord('q'): # wait for key stroke or 50ms
	# if k == 27:
		# break # break out of the loop

# cap.release() # release the capture stream



frame=C.imread("test.jpg")
#C.imshow('original',frame)
hsv=C.cvtColor(frame,C.COLOR_BGR2HSV)
#C.imshow('hsv',hsv)
msk1 = C.inRange(hsv, lower_blue, upper_blue)
msk2 = C.inRange(hsv, lower_red, upper_red)
msk3 = C.inRange(hsv, lower_green, upper_green)
msk=C.bitwise_or(msk1,msk2)
msk=C.bitwise_or(msk,msk3)
res = C.bitwise_and(frame,frame, mask= msk)
#C.imshow('mask',msk)
C.imshow('result',res)
C.waitKey(0)

	
C.destroyAllWindows()# close all the windows