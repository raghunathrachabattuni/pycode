import cv2
import numpy as np

road=cv2.imread("road1.jpg")
#print('full road')
#print(road)
#print('road[:,:,0]')
#print(road[:,:,0])
#print('road[:,:,1]')
#print(road[:,:,1])
#print('road[:,:,2]')
#print(road[:,:,2])
#cv2.imshow('original road',road)
print('this image is :',type(road),' with dimensions:',road.shape)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
selectedroad=np.copy(road)
selectedroad1=np.copy(road)
selectedroad2=np.copy(road)
ysize = road.shape[0]
xsize = road.shape[1]

#Below code is used to set threshold boundaries for the white image mask in BGR space
rth=200
gth=200
bth=200
rgbth=[bth,gth,rth]
thresholds=(road[:,:,0]<rgbth[0]) | (road[:,:,1]<rgbth[1]) | (road[:,:,2]<rgbth[2])

selectedroad[thresholds]=[0,0,0]
#cv2.imshow('selectedroad',selectedroad)
#Above code is used to set threshold boundaries for the white image mask in RGB space



#[0,0] is the upper-left corner
left_bottom=[100,539]
right_bottom=[900,539]
apex=[474,325]
fit_left = np.polyfit((left_bottom[0], apex[0]), (left_bottom[1], apex[1]), 1)
fit_right = np.polyfit((right_bottom[0], apex[0]), (right_bottom[1], apex[1]), 1)
fit_bottom = np.polyfit((left_bottom[0], right_bottom[0]), (left_bottom[1], right_bottom[1]), 1)
XX, YY = np.meshgrid(np.arange(0, xsize), np.arange(0, ysize))

thresholds1 = (YY > (XX*fit_left[0] + fit_left[1])) & \
                    (YY > (XX*fit_right[0] + fit_right[1])) & \
                    (YY < (XX*fit_bottom[0] + fit_bottom[1]))
thresholds1_inv = (YY > (XX*fit_left[0] + fit_left[1])) & \
                    (YY > (XX*fit_right[0] + fit_right[1])) & \
                    (YY < (XX*fit_bottom[0] + fit_bottom[1]))
selectedroad1[~thresholds1 ] = [0, 0, 0]
cv2.imshow('with triangular cut',selectedroad1)
selectedroad1[thresholds]=[0,0,0]

newthresholds=selectedroad1[:,:,1]<220

selectedroad2[~newthresholds]=[0,0,255]


cv2.imshow('with triangular mask',selectedroad2)


cv2.waitKey(0)
cv2.destroyAllWindows()
